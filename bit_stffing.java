import java.util.*;
public class Solution {
	
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		String bit_stream = scan.next();
		String flag = "01111110";
		int N = bit_stream.length();
		
		
		for(int i=4; i<N && N > 5; i++) {
			if(bit_stream.substring(i-4, i+1).equals("11111")) {
				bit_stream = bit_stream.substring(0, i+1)+"0"+bit_stream.substring(i+1,N);
			}
		}
		
		System.out.println(flag+bit_stream+flag);
		
	}

}
